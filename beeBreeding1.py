coordinates = [[1, 0, -1],[0, 1, -1],[-1, 1, 0],[-1, 0, 1],[0, -1, 1],[1, -1, 0]]

def isSpiral(cell_num1, cell_num2):
    if((cell_num1 > 0 and cell_num2 > 0) and (cell_num1 <= 10000 and cell_num2 <= 10000) and (cell_num1 != cell_num2)):
        return shortestDistance(cell_num1, cell_num2)

def shortestDistance(cell_num1, cell_num2):
    pass

def cellPlot(radius):

    cell_num = 1
    cellsDict = {1 : (0, 0, 0)}

    for rad in range(radius):
        x_coord = 0
        y_coord = -rad
        z_coord = rad

        for i in range(6):
            for j in range(rad):
                cell_num += 1
                x_coord += coordinates[i][0]
                y_coord += coordinates[i][1]
                z_coord += coordinates[i][2]
                cellsDict[cell_num] = (x_coord, y_coord, z_coord)

    return cellsDict

cell_num1, cell_num2 = map(int, input().split())
#print(isSpiral(cell_num1, cell_num2))
cellsDict = cellPlot(6)
for i in cellsDict.items():
    print(i)
